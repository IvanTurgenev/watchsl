# watchsl
## gotify notifications for new posts in multiple subreddits with specific keywords

[![pipeline status](https://gitlab.com/IvanTurgenev/watchsl/badges/master/pipeline.svg)](https://gitlab.com/IvanTurgenev/watchsl/-/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/IvanTurgenev/watchsl)](https://goreportcard.com/report/gitlab.com/IvanTurgenev/watchsl)

![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/GPLv3_Logo.svg/200px-GPLv3_Logo.svg.png "Title Text")
